INSERT INTO groupe_cinema (nom,logo,creeatedAt,updatedAt,tarif_normal,tarif_special,"condition") VALUES
	 ('CGR Cinéma','logocgr-673262799bc6a887501663.jpg','2024-11-11 20:00:57','2024-11-21 16:50:04',720.0,1020.0,'Valide 9 mois'),
	 ('Pathé','logopathe-673264133d096219426766.webp','2024-11-11 20:07:47','2024-11-21 16:50:27',990.0,NULL,'Validité 9 mois'),
	 ('Cineum','cineum-cannes-cine-1200px-674375761aa7c665594204.jpg','2024-11-24 18:50:30','2024-11-24 18:50:30',730.0,NULL,'Validité 9 mois'),
	 ('La Strada','cinema-la-strada-6743770a2828f241981334.jpg','2024-11-24 18:57:14','2024-11-24 18:57:14',610.0,NULL,'validité 3 ans');
