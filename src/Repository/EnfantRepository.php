<?php

namespace App\Repository;

use DateTime;
use Expr\Orx;
use App\Entity\Enfant;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Enfant>
 *
 * @method Enfant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enfant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enfant[]    findAll()
 * @method Enfant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnfantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Enfant::class);
    }


    public function getEnfant16ansNoel(bool $is16): array
    {

        // $y10 = (new DateTime())->sub(new \DateInterval('P10Y'))->format('Y-m-d');
        $y10 = (DateTime::createFromFormat("d/m/Y", '31/12/' . date('Y')))->sub(new \DateInterval('P10Y'))->format('Y-m-d');
        $y16 = (DateTime::createFromFormat("d/m/Y", '31/12/' . date('Y')))->sub(new \DateInterval('P16Y'))->format('Y-m-d');
        $d6 = (new DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        $d8 = (new DateTime())->sub(new \DateInterval('P8M'))->format('Y-m-d');

        $sql = $this->createQueryBuilder('e')
            ->select('a', 'e')
            ->leftJoin('e.agents', 'a')
            ->where('a.isActive = true');
        if (!$is16)
            $sql = $sql
                ->andWhere('e.dateNaissance > ?3');
        if ($is16)
            $sql = $sql
                ->andWhere('e.dateNaissance < ?3')
                ->andWhere('e.dateNaissance > ?4')
                ->setParameter(4, $y16);
        $sql = $sql
            ->andWhere(
                new Expr\Orx([
                    new Expr\Andx(
                        [
                            'a.isCDD = true',
                            'a.dateEmbauche < ?1'
                        ]
                    ),
                    new Expr\Andx(
                        [
                            'a.isCDD = false',
                            'a.dateEmbauche < ?2'
                        ]
                    )
                ])
            )
            ->setParameter(1, $d8)
            ->setParameter(2, $d6)
            ->setParameter(3, $y10)

            ->addOrderBy('a.site', 'ASC')
            ->addOrderBy('a.nom', 'ASC')
            ->getQuery();
        // dd($sql);


        return $sql->getResult();
    }

    public function getEnfant16ansRentree(bool $is16): array
    {

        // $y10 = (new DateTime())->sub(new \DateInterval('P10Y'))->format('Y-m-d');
        $y3 = (DateTime::createFromFormat("d/m/Y", '01/01/' . date('Y')))->sub(new \DateInterval('P3Y'))->format('Y-m-d');
        $y16 = (DateTime::createFromFormat("d/m/Y", '01/09/' . date('Y')))->sub(new \DateInterval('P16Y'))->format('Y-m-d');
        $y25 = (DateTime::createFromFormat("d/m/Y", '31/12/' . date('Y')))->sub(new \DateInterval('P25Y'))->format('Y-m-d');
        $d6 = (new DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        $d8 = (new DateTime())->sub(new \DateInterval('P8M'))->format('Y-m-d');

        $sql = $this->createQueryBuilder('e')
            ->select('a', 'e')
            ->leftJoin('e.agents', 'a')
            ->where('a.isActive = true');
        if (!$is16)
            $sql = $sql
                ->andWhere('e.dateNaissance < ?3')
                ->andWhere('e.dateNaissance > ?4')
                ->setParameter(3, $y3);
        if ($is16)
            $sql = $sql
                ->andWhere('e.dateNaissance < ?4')
                ->andWhere('e.dateNaissance > ?3')

                ->setParameter(3, $y25);
        $sql = $sql
            ->andWhere(
                new Expr\Orx([
                    new Expr\Andx(
                        [
                            'a.isCDD = true',
                            'a.dateEmbauche < ?1'
                        ]
                    ),
                    new Expr\Andx(
                        [
                            'a.isCDD = false',
                            'a.dateEmbauche < ?2'
                        ]
                    )
                ])
            )
            ->setParameter(1, $d8)
            ->setParameter(2, $d6)
            ->setParameter(4, $y16)



            ->addOrderBy('a.site', 'ASC')
            ->addOrderBy('a.nom', 'ASC')
            ->getQuery();
        // dd($sql);


        return $sql->getResult();
    }

    //    /**
    //     * @return Enfant[] Returns an array of Enfant objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Enfant
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
