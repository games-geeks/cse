<?php

namespace App\Repository;

use App\Entity\StatusMarital;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StatusMarital>
 *
 * @method StatusMarital|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusMarital|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusMarital[]    findAll()
 * @method StatusMarital[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusMaritalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusMarital::class);
    }

    //    /**
    //     * @return StatusMarital[] Returns an array of StatusMarital objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?StatusMarital
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
