<?php

namespace App\Repository;

use DateTime;
use App\Entity\Agent;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Agent>
 *
 * @method Agent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Agent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Agent[]    findAll()
 * @method Agent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private PaginatorInterface $paginator)
    {
        parent::__construct($registry, Agent::class);
    }

    public function getAgent(bool $isParent = false): array
    {

        $d6 = (new DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        $d8 = (new DateTime())->sub(new \DateInterval('P8M'))->format('Y-m-d');

        $sql = $this->createQueryBuilder('a')
            ->select('a', 'e')
            ->leftJoin('a.enfant', 'e')
            ->where('a.isActive = true')
            ->andWhere(
                new Expr\Orx([
                    new Expr\Andx(
                        [
                            'a.isCDD = true',
                            'a.dateEmbauche < ?1'
                        ]
                    ),
                    new Expr\Andx(
                        [
                            'a.isCDD = false',
                            'a.dateEmbauche < ?2'
                        ]
                    )
                ])
            )
            ->setParameter(1, $d8)
            ->setParameter(2, $d6);
        if ($isParent)
            $sql = $sql
                ->andWhere('a MEMBER OF e.agents');
        $sql = $sql
            ->addOrderBy('a.site', 'ASC')
            ->addOrderBy('a.nom', 'ASC')
            ->getQuery();
        // dd($sql);


        return $sql->getResult();
    }


    public function getAgentANCV(): array
    {

        $d6 = (new DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        $d8 = (new DateTime())->sub(new \DateInterval('P8M'))->format('Y-m-d');
        $y25 = (DateTime::createFromFormat("d/m/Y", '01/03/' . date('Y')))->sub(new \DateInterval('P25Y'))->format('Y-m-d');

        $sql = $this->createQueryBuilder('a')
            ->select('a', 'e')
            ->leftJoin('a.enfant', 'e')
            ->where('a.isActive = true')
            ->andWhere(
                new Expr\Orx([
                    new Expr\Andx(
                        [
                            'a.isCDD = true',
                            'a.dateEmbauche < ?1'
                        ]
                    ),
                    new Expr\Andx(
                        [
                            'a.isCDD = false',
                            'a.dateEmbauche < ?2'
                        ]
                    )
                ])
            )
            ->andWhere('e.dateNaissance > ?3')

            ->setParameter(3, $y25)
            ->setParameter(1, $d8)
            ->setParameter(2, $d6);

        $sql = $sql
            ->addOrderBy('a.site', 'ASC')
            ->addOrderBy('a.nom', 'ASC')
            ->getQuery();
        // dd($sql);


        return $sql->getResult();
    }



    public function paginateUser(int $page): PaginationInterface
    {
        $builder = $this->createQueryBuilder('r');
        return $this->paginator->paginate(
            $builder,
            $page,
            10,
            [
                'distinct' => false,
                'sortFieldAllowList' => ['r.id', 'r.nom', 'r.site']
            ]
        );
    }

    //    /**
    //     * @return Agent[] Returns an array of Agent objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Agent
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
