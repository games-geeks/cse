<?php

namespace App\Service;

use DateTime;
use App\Entity\City;
use App\Entity\Agent;
use App\Entity\Enfant;
use League\Csv\Reader;
use App\Repository\SiteRepository;
use App\Repository\AgentRepository;
use App\Repository\EnfantRepository;
use App\Repository\StatusMaritalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportFamilleService
{
    public function __construct(
        private AgentRepository $agentRepository,
        private SiteRepository $siteRepository,
        private EnfantRepository $enfantRepository,
        private StatusMaritalRepository $statusMaritalRepository,
        private EntityManagerInterface $em
    ) {
    }
    public function importFamilles(SymfonyStyle $io): void
    {
        $io->title('Importation des Familles');
        $familles = $this->readCsvFile();
        $io->progressStart(count($familles));

        foreach ($familles as $arrayFamilles) {
            // dd($arrayFamilles);
            $io->progressAdvance();
            if ($arrayFamilles['top'] == 1) {
                $famille  = $this->createOrUpdateFamille($arrayFamilles);
                $this->em->persist($famille);
                for ($i = 1; $i < 7; $i++)
                    if (strlen($arrayFamilles['enf' . $i]) > 0) {
                        $enfant  = $this->createOrUpdateEnfant($arrayFamilles, $famille, $i);
                        $this->em->persist($enfant);
                    }
            }
        }
        $this->em->flush();
        $io->progressFinish();
        $io->success('Importation terminée');
    }

    private function readCsvFile(): Reader
    {
        $csv = Reader::createFromPath('%kernel.root_dir%/../import/famille.csv', 'r');
        $csv->setHeaderOffset(0);
        return $csv;
    }

    private function createOrUpdateFamille(array $arrayFamille): Agent
    {
        $famille = new Agent;
        echo $arrayFamille['nom'];
        $famille->setMatricule((int) $this->ifIsset($arrayFamille['matricule'], 'int'));
        $famille->setSite($this->siteRepository->findOneBy(['nom' =>  $arrayFamille['site']]));
        $famille->setStatus($this->statusMaritalRepository->findOneBy(['nom' =>  ucfirst($arrayFamille['marital'])]));
        $famille->setNom($arrayFamille['nom']);
        $famille->setPrenom($arrayFamille['prenom']);
        if ($arrayFamille['nsi'] != '')
            $famille->setDateNaissance($this->stringToDatetime($arrayFamille['nsi']));
        if ($arrayFamille['emb'] != '')
            $famille->setDateEmbauche($this->stringToDatetime($arrayFamille['emb']));
        $famille->setEmail($this->ifIsset($arrayFamille['mail']));
        $famille->setTelephone($this->ifIsset($arrayFamille['tel']));
        $famille->setAyantDroit($this->ifIsset($arrayFamille['conjoint']));
        if ($arrayFamille['nsic'] != '')
            $famille->setDateNaissanceAD($this->stringToDatetime($arrayFamille['nsic']));
        $famille->setIsActive(true);
        $famille->setAdresse($this->ifIsset($arrayFamille['adress']));
        $famille->setCodePostal($this->ifIsset($arrayFamille['cp']));
        $famille->setVille($this->ifIsset($arrayFamille['ville']));
        $famille->setIsCDD($arrayFamille['cdd']);
        // dd($famille);
        return $famille;
    }

    private function createOrUpdateEnfant(array $arrayFamille, Agent $agent, int $num): Enfant
    {
        $famille = new Enfant;
        list($nom, $prenom) = explode(" ", $arrayFamille['enf' . $num]);
        $famille->setNom($nom);
        $famille->setPrenom($prenom);
        // if ($num > 2)        dd($famille);
        echo $arrayFamille['enf' . $num];
        $famille->setDateNaissance($this->stringToDatetime($arrayFamille['nsi' . $num]));

        $famille->addAgent($agent);


        // dd($famille);
        return $famille;
    }

    /**
     * Teste si la propriété existe ou non
     */
    private function ifIsset(?string $value, string $type = 'string')
    {
        if (isset($value))
            if (strlen($value) != 0)
                return $value;
            else {
                if ($type == 'int')
                    return 0;
                else
                    return "";
            }
    }
    /**
     * Fonction permettant de retourner un timestamp à parti d'un string
     */
    private function stringToDatetime(string $sDate): DateTime
    {

        $dtime = DateTime::createFromFormat("d/m/y", $sDate);
        $timestamp = $dtime->getTimestamp();
        echo $dtime->format('d/m/y');
        var_dump($dtime->getTimestamp());
        $dt = new DateTime();
        return $dt->setTimestamp($timestamp);
    }
}


// "top" => "1"
// "matricule" => "307"
// "site" => "Marseille"
// "marital" => "concubinage"
// "nom" => "BOITE"
// "prenom" => "Gilles"
// "nsi" => "22/02/70"
// "enf" => "1"
// "enf1" => "BOITE Margaux"
// "nsi1" => "12/07/00"
// "age1" => "23,4"
// "enf2" => "BOITE Adrien"
// "ns2" => "14/04/04"
// "age2" => "19,6"
// "enf3" => "PRIN-ABEIL Léonie"
// "ns3" => "24/6/2008"
// "age3" => "15,4"
// "enf4" => ""
// "ns4" => ""
// "age4" => ""
// "enf5" => ""
// "ns5" => ""
// "age5" => ""
// "enf6" => ""
// "ns6" => ""
// "age6" => ""
// "adress" => "31 rue de Canadel - 13013 Marseille"
// "mail" => "gilles.b@free.fr"
// "cdd" => ""
// "emb" => "30/09/99"
// "fin" => ""
// "null" => ""
// "conjoint" => "LABALME Murielle"
// "nsic" => "13/08/73"