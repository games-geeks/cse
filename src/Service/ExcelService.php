<?php

declare(strict_types=1);

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelService
{


    public function __construct()
    {
    }


    public function getExcelAgent(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;
        $endLetter = 'AI';
        $alphas = range('A', 'Z');
        // On met les bordures
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':' . $endLetter . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        //on met les filtres
        $spreadsheet->getActiveSheet()->setAutoFilter('B' . $start . ':' . $endLetter . count($agents) + 3);
        //on met les colonnes en autosize
        foreach ($alphas as $alpha) {
            $spreadsheet->getActiveSheet()->getColumnDimension($alpha)->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('A' . $alpha)->setAutoSize(true);
        }

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('D' . $start . ':' . $endLetter . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':' . $endLetter . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Liste des agents au ' . date("Y-m-d"));


        $sheet->getCell('B' . $start)->setValue('Site');
        $sheet->getCell('C' . $start)->setValue('Matricule');
        $sheet->getCell('D' . $start)->setValue('Nom');
        $sheet->getCell('E' . $start)->setValue('Prenom');
        $sheet->getCell('F' . $start)->setValue('Date de Naissance');
        $sheet->getCell('G' . $start)->setValue('Email');
        $sheet->getCell('H' . $start)->setValue('Adresse');
        $sheet->getCell('I' . $start)->setValue('Code Postal');
        $sheet->getCell('J' . $start)->setValue('Ville');
        $sheet->getCell('K' . $start)->setValue('Telephone');
        $sheet->getCell('L' . $start)->setValue('Date Embauche');
        $sheet->getCell('M' . $start)->setValue('CDD?');
        $sheet->getCell('N' . $start)->setValue('Fin Emabauche');
        $sheet->getCell('O' . $start)->setValue('Conjoint');
        $sheet->getCell('P' . $start)->setValue('Date Conjoint');
        $sheet->getCell('Q' . $start)->setValue('Staut');
        $sheet->getCell('R' . $start)->setValue('Enfant 1');
        $sheet->getCell('S' . $start)->setValue('Naissance 1');
        $sheet->getCell('T' . $start)->setValue('Age 1');
        $sheet->getCell('U' . $start)->setValue('Enfant 2');
        $sheet->getCell('V' . $start)->setValue('Naissance 2');
        $sheet->getCell('W' . $start)->setValue('Age 2');
        $sheet->getCell('X' . $start)->setValue('Enfant 3');
        $sheet->getCell('Y' . $start)->setValue('Naissance 3');
        $sheet->getCell('Z' . $start)->setValue('Age 3');
        $sheet->getCell('AA' . $start)->setValue('Enfant 4');
        $sheet->getCell('AB' . $start)->setValue('Naissance 4');
        $sheet->getCell('AC' . $start)->setValue('Age 4');
        $sheet->getCell('AD' . $start)->setValue('Enfant 5');
        $sheet->getCell('AE' . $start)->setValue('Naissance 5');
        $sheet->getCell('AF' . $start)->setValue('Age 5');
        $sheet->getCell('AG' . $start)->setValue('Enfant 6');
        $sheet->getCell('AH' . $start)->setValue('Naissance 6');
        $sheet->getCell('AI' . $start)->setValue('Age 6');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);
        // $sheet->setCellValue('AD4', '=(AUJOURDHUI()-S4 )');
        //on calcule l'age des enfants
        for ($i = $start + 1; $i <= count($agents) + 3; $i++) {
            $this->setCellAge($sheet, 'S', 'T', $i);
            $this->setCellAge($sheet, 'V', 'W', $i);
            $this->setCellAge($sheet, 'Y', 'Z', $i);
            $this->setCellAge($sheet, 'AB', 'AC', $i);
            $this->setCellAge($sheet, 'AE', 'AF', $i);
            $this->setCellAge($sheet, 'AH', 'AI', $i);
        }

        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }

    private function setCellAge(Worksheet $sheet, String $debut, String $fin, int $position)
    {
        $value = $sheet->getCell($debut . $position)->getValue();
        if ($value != '') {
            $sheet->setCellValue($fin . $position, '=(TODAY() -' . $debut . $position . ')/365');
            $sheet->getStyle($fin . $position)->getNumberFormat()->setFormatCode('#,##0.00');
        }
    }


    public function getExcelParent(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;

        // On met les bordure
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':G' . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        //on met les colonnes B C D Een autosize
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

        //le reste à une longueur fixe
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(24);

        //et la hauteur
        for ($i = 4; $i < count($agents) + 4; $i++)
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(50);

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('D' . $start . ':E' . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':E' . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('B' . $start)->setValue('Nom');
        $sheet->getCell('C' . $start)->setValue('Prénom');
        $sheet->getCell('D' . $start)->setValue('Site');
        $sheet->getCell('E' . $start)->setValue('Montant');
        $sheet->getCell('F' . $start)->setValue('Date');
        $sheet->getCell('G' . $start)->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);



        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }

    public function getExcelEnfant(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;

        // On met les bordure
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':O' . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        //on met les colonnes B C D Een autosize
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);


        //le reste à une longueur fixe
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(24);

        //et la hauteur
        for ($i = 4; $i < count($agents) + 4; $i++)
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(50);

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':O' . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':O' . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('B' . $start)->setValue('Site');
        $sheet->getCell('C' . $start)->setValue('Parent');
        $sheet->getCell('D' . $start)->setValue('Nom');
        $sheet->getCell('E' . $start)->setValue('Prénom');
        $sheet->getCell('F' . $start)->setValue('Date Naissance');
        $sheet->getCell('G' . $start)->setValue('Age');
        $sheet->getCell('H' . $start)->setValue('Reférence 1');
        $sheet->getCell('I' . $start)->setValue('Page 1');
        $sheet->getCell('J' . $start)->setValue('Prix 1');
        $sheet->getCell('K' . $start)->setValue('Reférence 2');
        $sheet->getCell('L' . $start)->setValue('Page 2');
        $sheet->getCell('M' . $start)->setValue('Prix 2');
        $sheet->getCell('N' . $start)->setValue('Date');
        $sheet->getCell('O' . $start)->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);



        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }

    public function getExcelEnfantCarteNoel(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;

        // On met les bordure
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':J' . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        //on met les colonnes B C D Een autosize
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        //le reste à une longueur fixe
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(24);

        //et la hauteur
        for ($i = 4; $i < count($agents) + 4; $i++)
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(50);

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':J' . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':J' . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('B' . $start)->setValue('Site');
        $sheet->getCell('C' . $start)->setValue('Parent');
        $sheet->getCell('D' . $start)->setValue('Nom');
        $sheet->getCell('E' . $start)->setValue('Prénom');
        $sheet->getCell('F' . $start)->setValue('Date Naissance');
        $sheet->getCell('G' . $start)->setValue('Age');
        $sheet->getCell('H' . $start)->setValue('Montant');
        $sheet->getCell('I' . $start)->setValue('Date');
        $sheet->getCell('J' . $start)->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);



        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }

    public function getExcelEnfantCarteRentree16(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;

        // On met les bordure
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':J' . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        //on met les colonnes B C D Een autosize
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        //le reste à une longueur fixe
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(24);

        //et la hauteur
        for ($i = 4; $i < count($agents) + 4; $i++)
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(50);

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':L' . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':L' . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('B' . $start)->setValue('Site');
        $sheet->getCell('C' . $start)->setValue('Parent');
        $sheet->getCell('D' . $start)->setValue('Nom');
        $sheet->getCell('E' . $start)->setValue('Prénom');
        $sheet->getCell('F' . $start)->setValue('Date Naissance');
        $sheet->getCell('G' . $start)->setValue('Age');
        $sheet->getCell('H' . $start)->setValue('Classe');
        $sheet->getCell('I' . $start)->setValue('Montant');
        $sheet->getCell('J' . $start)->setValue('Certificat');
        $sheet->getCell('K' . $start)->setValue('Date');
        $sheet->getCell('L' . $start)->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);



        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }


    public function getExcelANCV(array $list, array $agents, string $name): void
    {
        $spreadsheet = new Spreadsheet();
        $start = 3;

        // On met les bordure
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':G' . count($agents) + 3)
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        //on met les colonnes B C D Een autosize
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

        //le reste à une longueur fixe

        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(24);


        //et la hauteur
        for ($i = 4; $i < count($agents) + 4; $i++)
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(50);

        //alignement
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':G' . count($agents) + 3)
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B' . $start . ':G' . count($agents) + 3)
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('B' . $start)->setValue('Site');
        $sheet->getCell('C' . $start)->setValue('Nom');
        $sheet->getCell('D' . $start)->setValue('Prénom');
        $sheet->getCell('E' . $start)->setValue('Montant');
        $sheet->getCell('F' . $start)->setValue('Date');
        $sheet->getCell('G' . $start)->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'B' . $start + 1, true);



        $writer = new Xlsx($spreadsheet);

        $writer->save('Excel/' . $name);
    }
}
