<?php

namespace App\Service;

use DOMXPath;
use DOMDocument;
use GuzzleHttp\Client;



class ScrapCinema
{

    public static function scrap_infoFilm($url, $cinema)
    {
        $films = [];
        $client = new Client(['timeout' => 5]);
        $response = $client->get($url);

        $html = $response->getBody()->getContents();

        // Chargement de la page HTML dans DOMDocument
        $dom = new DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new DOMXPath($dom);

        $films_list_container = $xpath->query('//div[contains(@class, "showtimes-list-holder")]');

        if ($films_list_container->length > 0) {
            $films_list = $xpath->query('.//div[contains(@class, "card entity-card entity-card-list movie-card-theater cf hred")]', $films_list_container->item(0));

            foreach ($films_list as $film) {
                $titre = $xpath->query('.//div[contains(@class, "meta")]//h2[contains(@class, "meta-title")]/a', $film)->item(0)->nodeValue;

                $realisateur_section = $xpath->query('.//div[contains(@class, "meta-body-item meta-body-direction")]', $film)->item(0);

                $realisateur = $realisateur_section ? $xpath->query('.//span[contains(@class, "dark-grey-link")]', $realisateur_section)->item(0)->nodeValue : "Réalisateur non trouvé";

                // $dataFilm_firebase = recupererDataFilm($titre, $realisateur);
                // if (!$dataFilm_firebase) {
                // Extraction de l'image
                $img_url = '';
                $thumbnail_img = $xpath->query('.//img[contains(@class, "thumbnail-img")]', $film)->item(0);
                if ($thumbnail_img && strpos($thumbnail_img->getAttribute('src'), 'data:image') === false) {
                    $img_url = $thumbnail_img->getAttribute('src');
                } else {
                    $urlAffiche = "https://www.allocine.fr" . $xpath->query('.//div[contains(@class, "meta")]//h2[contains(@class, "meta-title")]/a', $film)->item(0)->getAttribute('href');
                    $responseAffiche = $client->get($urlAffiche);
                    $pageFilmHtml = $responseAffiche->getBody()->getContents();
                    $pageFilmDom = new DOMDocument();
                    @$pageFilmDom->loadHTML($pageFilmHtml);
                    $pageFilmXpath = new DOMXPath($pageFilmDom);
                    $thumbnail_img = $pageFilmXpath->query('.//img[contains(@class, "thumbnail-img")]')->item(0);
                    $img_url = $thumbnail_img && strpos($thumbnail_img->getAttribute('src'), 'data:image') === false ? $thumbnail_img->getAttribute('src') : 'Image de la vignette non trouvée';
                }

                $synopsis = $xpath->query('.//div[contains(@class, "synopsis")]//div[contains(@class, "content-txt")]', $film)->item(0)->nodeValue ?: "synopsis non trouvé";
                $acteur_container = $xpath->query('.//div[contains(@class, "meta-body-item meta-body-actor")]', $film)->item(0);
                $acteurs = [];
                if ($acteur_container) {
                    foreach ($xpath->query('.//span[contains(@class, "dark-grey-link")]', $acteur_container) as $acteur) {
                        $acteurs[] = $acteur->nodeValue;
                    }
                } else {
                    $acteurs = ["acteurs non trouvés"];
                }

                $horaire_sections = $xpath->query('.//div[contains(@class, "showtimes-hour-block")]', $film);
                $horaires = [];
                foreach ($horaire_sections as $horaire_section) {
                    $horaire = $xpath->query('.//span[contains(@class, "showtimes-hour-item-value")]', $horaire_section)->item(0);
                    if ($horaire) {
                        $horaires[] = $horaire->nodeValue;
                    }
                }
                if (empty($horaires)) $horaires = ["Horaire non trouvé"];

                $genre_container = $xpath->query('.//div[contains(@class, "meta-body-item meta-body-info")]', $film)->item(0);
                $genres = [];
                if ($genre_container) {
                    foreach ($xpath->query('.//span', $genre_container) as $span) {
                        if (!$span->hasAttribute('class') || (strpos($span->getAttribute('class'), 'spacer') === false && strpos($span->getAttribute('class'), 'nationality') === false)) {
                            $genres[] = trim($span->nodeValue);
                        }
                    }
                    array_shift($genres);  // Suppression de la première entrée
                } else {
                    $genres = ["Genre non trouvé"];
                }

                // Récupération de la durée du film via TMDB API
                $tmdb_api_url = "https://api.themoviedb.org/3/search/movie?query=" . urlencode($titre);
                $tmdb_headers = [
                    'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlYjIxZmM3MTQxNTNmZjMxY2RlNTMxZmY5ZjRiZjk1MyIsIm5iZiI6MTczMTQ0NTUwMi41ODE1Nzk0LCJzdWIiOiI2NzMzYzFiNTljMWEyMzhkOGE5ZDI1ZjUiLCJzY29wZXMiOlsiYXBpX3JlYWQiXSwidmVyc2lvbiI6MX0.v4ChrfFIcvis-QW0BL1jfSPsh_4fecQta4c_JD_KOuA',
                    'accept' => 'application/json',
                ];

                $response_tmdb = $client->get($tmdb_api_url, ['headers' => $tmdb_headers]);
                $duree_film = 0;
                if ($response_tmdb->getStatusCode() == 200) {
                    $data_tmdb = json_decode($response_tmdb->getBody(), true);
                    if (isset($data_tmdb['results'][0])) {
                        $film_id = $data_tmdb['results'][0]['id'];
                        $film_details_url = "https://api.themoviedb.org/3/movie/" . $film_id;
                        $response_film_details = $client->get($film_details_url, ['headers' => $tmdb_headers]);
                        $data_film_details = json_decode($response_film_details->getBody(), true);
                        $duree_film = $data_film_details['runtime'];
                    }
                }
                $heure = floor($duree_film / 60);
                $minute = $duree_film % 60;

                $film_data = [
                    "titre" => $titre,
                    "realisateur" => $realisateur,
                    "casting" => $acteurs,
                    "genres" => $genres,
                    "duree" => ["heure" => $heure, "minute" => $minute],
                    "affiche" => $img_url,
                    "synopsis" => $synopsis,
                    "horaires" => [
                        [
                            "cinema" => $cinema,
                            "seances" => $horaires
                        ]
                    ]
                ];
                array_push($films, $film_data);
                // enregistrementFilm($film_data);
                //echo $film_data['titre'] . " : enregistré dans la db\n";
                // }
            }
        }
        // dd($films);
        return $films;
    }
}
