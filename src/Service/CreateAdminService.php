<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Membre;
use App\Repository\MembreRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(
        private readonly MembreRepository $membreRepository,
        private readonly UserPasswordHasherInterface $userPasswordHasher
    ) {
    }

    public function create(string $email, string $password): void
    {
        $user = $this->membreRepository->findOneBy(['email' => $email]);

        // On crée user si il n'existe pas
        if (!$user) {
            $user = new Membre();
            $user->setEmail($email);
            $password = $this->userPasswordHasher->hashPassword($user, $password);
            $user->setPassword($password);
        }
        // Dans tous les cas, on lui donne les droits admin
        $user->setRoles(['ROLE_ADMIN']);
        $this->membreRepository->save($user, true);
    }
}
