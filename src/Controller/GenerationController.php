<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenerationController extends AbstractController
{
    #[Route('/generation', name: 'generation.index')]
    public function index(): Response
    {
        return $this->render('generation/index.html.twig', [
            'controller_name' => 'GenerationController',
        ]);
    }
}
