<?php

namespace App\Controller\Admin;

use App\Entity\StatusMarital;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StatusMaritalCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StatusMarital::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('nom'),

        ];
    }
}
