<?php

namespace App\Controller\Admin;

use App\Entity\SalleCinema;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SalleCinemaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SalleCinema::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('nom'),
            TextField::new('typeSalle'),
            IntegerField::new('placesRestantes'),
            MoneyField::new('prix')->setCurrency('EUR')->setNumDecimals(2),
            AssociationField::new('cinema'),
        ];
    }
}
