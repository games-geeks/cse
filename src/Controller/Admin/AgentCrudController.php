<?php

namespace App\Controller\Admin;

use App\Entity\Agent;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;

class AgentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Agent::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            EmailField::new('email')->onlyOnForms(),
            // TextField::new('password')
            //     ->setFormType(PasswordType::class)
            //     ->setRequired(false)
            //     ->onlyOnForms(),
            AssociationField::new('site'),
            IntegerField::new('matricule'),
            BooleanField::new('isCDD'),
            TextField::new('prenom'),
            TextField::new('nom'),
            DateField::new('dateEmbauche')->setFormat("dd/MM/yyyy"),
            TextField::new('adresse')->onlyOnForms(),
            TextField::new('codePostal')->onlyOnForms(),
            TextField::new('ville')->onlyOnForms(),
            TextField::new('telephone')->onlyOnForms(),
            DateField::new('dateNaissance')->setFormat("dd/MM/yyyy"),
            AssociationField::new('status'),
            AssociationField::new('enfant'),
            TextField::new('ayantDroit'),
            DateField::new('dateNaissanceAD')->setFormat("dd/MM/yyyy")->onlyOnForms(),
            BooleanField::new('isActive'),
            DateField::new('finContrat')->setFormat("dd/MM/yyyy"),

        ];
    }
}
