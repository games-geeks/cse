<?php

namespace App\Controller\Admin;

use App\Entity\Enfant;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;

class EnfantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Enfant::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('prenom'),
            TextField::new('nom'),
            DateField::new('dateNaissance'),
            AssociationField::new('agents')->onlyOnForms(),
            ArrayField::new('agents')->onlyOnIndex(),
            BooleanField::new('hasCertificat'),
        ];
    }
}
