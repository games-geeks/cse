<?php

namespace App\Controller\Admin;

use App\Entity\Site;
use App\Entity\Agent;
use App\Entity\Cinema;
use App\Entity\Enfant;
use App\Entity\Membre;
use App\Entity\Raison;
use App\Entity\Montant;
use App\Entity\Commande;
use App\Entity\Restriction;
use App\Entity\SalleCinema;
use App\Entity\GroupeCinema;
use App\Entity\StatusMarital;
use App\Entity\StatutCommande;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(AgentCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('CSE');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Agent & Famille');
        yield MenuItem::linkToCrud('Membre CSE', 'fas fa-star', Membre::class);
        yield MenuItem::linkToCrud('Agent', 'fas fa-envelope', Agent::class);
        yield MenuItem::linkToCrud('Enfant', 'fas fa-book', Enfant::class);
        yield MenuItem::linkToCrud('Site', 'fas fa-user-pen', Site::class);
        yield MenuItem::linkToCrud('StatusMarital', 'fas fa-building', StatusMarital::class);
        yield MenuItem::section('Occasion');
        yield MenuItem::linkToCrud('Raison', 'fas fa-star', Raison::class);
        yield MenuItem::linkToCrud('Montant', 'fas fa-star', Montant::class);
        yield MenuItem::linkToCrud('Restriction', 'fas fa-star', Restriction::class);
        yield MenuItem::section('Cinema');
        yield MenuItem::linkToCrud('GroupeCinema', 'fas fa-star', GroupeCinema::class);
        yield MenuItem::linkToCrud('Cinema', 'fa-solid fa-film', Cinema::class);
        yield MenuItem::linkToCrud('SalleCinema', 'fa fa-film', SalleCinema::class);
        yield MenuItem::linkToCrud('StatutCommande', 'fa fa-film', StatutCommande::class);
        yield MenuItem::section('Liste des commandes');
        yield MenuItem::linkToCrud('Commande', 'fa fa-film', Commande::class);
    }
}
