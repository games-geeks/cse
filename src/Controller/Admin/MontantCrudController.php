<?php

namespace App\Controller\Admin;

use App\Entity\Montant;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class MontantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Montant::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            AssociationField::new('raison'),
            AssociationField::new('condition'),
            IntegerField::new('prix'),
        ];
    }
}
