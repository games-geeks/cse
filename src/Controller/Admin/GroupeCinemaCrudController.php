<?php

namespace App\Controller\Admin;

use App\Entity\GroupeCinema;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;


class GroupeCinemaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return GroupeCinema::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('nom'),
            MoneyField::new('tarifNormal')->setCurrency('EUR')->setNumDecimals(2),
            MoneyField::new('tarifSpecial')->setCurrency('EUR')->setNumDecimals(2),
            TextField::new('condition'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('logo')->setBasePath('/uploads/logo')->onlyOnIndex(),
            //TextEditorField::new('description'),
        ];
    }
}
