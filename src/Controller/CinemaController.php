<?php

namespace App\Controller;

use App\Service\ScrapCinema;
use App\Repository\CinemaRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/cinema', name: 'cinema.')]
class CinemaController extends AbstractController
{
    #[Route('/programmation', name: 'programmation')]
    public function programmationFilm(CinemaRepository $cinemaRepository): Response
    {
        // Exemple d'utilisation pour récupérer les données
        $cinemas = $cinemaRepository->findBy([], ['nom' => 'ASC']);
        $films[] = array();

        foreach ($cinemas as $cinema) {
            $result = ScrapCinema::scrap_infoFilm($cinema->getUrlAlloCine(), $cinema->getNom());
            $films = array_merge($films, $result);
        }
        // dd($films);
        return $this->render('cinema/index.html.twig', [
            'films' => $films,
        ]);
    }
}
