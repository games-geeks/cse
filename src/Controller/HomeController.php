<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Service\ScrapCinema;
use App\Repository\AgentRepository;
use App\Repository\CinemaRepository;
use App\Repository\CommandeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app.home')]
    public function index(AgentRepository $agentRepository, CommandeRepository $commandeRepository): Response
    {
        $agent = new Agent();
        if ($this->getUser())
            $agent = $agentRepository->findOneBy(['matricule' => $this->getUser()->getMatricule()]);
        //récupération des commandes
        $commandes = $commandeRepository->findAll();
        return $this->render('home/index.html.twig', [
            'agent' => $agent,
            'commandes' => $commandes
        ]);
    }
}
