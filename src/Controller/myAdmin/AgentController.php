<?php

namespace App\Controller\myAdmin;

use DateTime;
use App\Entity\Agent;
use App\Form\AgentType;
use App\Repository\AgentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/agent', name: 'myadmin.agent.')]
class AgentController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(Request $request, AgentRepository $agentRepository): Response
    {
        //   $agents = $agentRepository->findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $agents = $agentRepository->paginateUser($page);
        return $this->render('myAdmin/agent/index.html.twig', compact('agents'));
    }

    #[Route('/dash', name: 'index')]
    public function index(): Response
    {
        //   $agents = $agentRepository->findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        // $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        // $agents = $agentRepository->paginateUser($page);
        return $this->render('myAdmin/agent/dashboard.html.twig', [
            'entity' => Agent::class,
            'headers' => ['Nom', 'Prenom', 'Email', 'Telephone',  'Matricule', 'Site', 'Status', 'isActive', 'isCDD', 'nbPlaceCinema'],
        ]);
    }

    // public function index(Request $request, CoachRepository $userRepository, Security $security): Response
    // {
    //     $page = $request->query->getInt('page', 1);
    //     // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
    //     $coachs = $userRepository->paginateUser($page, 'COACH');
    //     return $this->render('myAdmin/coach/index.html.twig', compact('coachs'));
    // }

    #[Route('/creation', name: 'create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $agent =  new Agent;
        $form = $this->createForm(AgentType::class, $agent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($agent);
            $em->flush();
            $this->addFlash('success', 'L\'agent a été créé');
            return $this->redirectToRoute('myadmin.agent.list');
        }
        return $this->render(
            'myAdmin/agent/add.html.twig',
            compact('form')
        );
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        Agent $agent,
        AgentRepository $agentRepository
    ): Response {
        $form = $this->createForm(AgentType::class, $agent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'L\'agent a été modifié');
            return $this->redirectToRoute('myadmin.agent.list');
        }
        return $this->render(
            'myAdmin/agent/edit.html.twig',
            compact('agent', 'form')
        );
    }

    #[Route('/cinema', name: 'cinema')]
    public function cinema(Request $request, AgentRepository $agentRepository, EntityManagerInterface $em): Response
    {
        // On parcourt tous les agents
        // $agents = $agentRepository->findAll();
        // foreach ($agents as $agent) {
        //     if ($agent->isIsActive()) {
        //         if (!$agent->getNbPlaceCinema()) {
        //             $d12 = (new DateTime())->sub(new \DateInterval('P1Y'))->format('Y-m-d');
        //             $nbEnfant = $agent->getEnfant()->count();
        //             $agent->setNbPlaceCinema(10 + $nbEnfant * 5);
        //             $agent->setUpdatedAt(new \DateTimeImmutable());
        //             $em->persist($agent);
        //         }
        //     }
        // }
        // $em->flush();
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $agents = $agentRepository->paginateUser($page);
        return $this->render('myAdmin/agent/index.html.twig', compact('agents'));
    }


    // #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    // // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    // public function delete(Coach $coach,  EntityManagerInterface $em)
    // {
    //     $message = 'Le coach a été supprimé';
    //     $em->remove($coach);
    //     $em->flush();

    //     $this->addFlash('success', $message);
    //     return $this->redirectToRoute('myadmin.coach.list');
    // }

    // #[Route('/up/{id}', name: 'promote', requirements: ['id' => Requirement::DIGITS], methods: ['GET'])]
    // // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    // public function promote(Coach $coach,  EntityManagerInterface $em)
    // {
    //     $coach->setRoles(['ROLE_SUPER_COACH']);
    //     $em->flush();

    //     $this->addFlash('success', 'Le coach a bien le nouveau role');
    //     return $this->redirectToRoute('myadmin.coach.list');
    // }
    // #[Route('/down/{id}', name: 'decrease', requirements: ['id' => Requirement::DIGITS], methods: ['GET'])]
    // // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    // public function decrease(Coach $coach,  EntityManagerInterface $em)
    // {

    //     $coach->setRoles(['ROLE_COACH']);
    //     $em->flush();

    //     $this->addFlash('success', 'Les roles ont bien été modifiés');
    //     return $this->redirectToRoute('myadmin.coach.list');
    // }
}
