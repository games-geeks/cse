<?php

namespace App\Controller;

use App\Repository\AgentRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AgentController extends AbstractController
{
    #[Route('/agent', name: 'app_agent')]
    public function index(AgentRepository $agentRepository): Response
    {
        $agents = $agentRepository->findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];
        foreach ($agents as $agent) {
            $list[] = [
                $agent->getNom(),
                $agent->getPrenom(),
                $agent->getSite(),
                '',
                '',
                ''
            ];
        }

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('User List');

        $sheet->getCell('A1')->setValue('Nom');
        $sheet->getCell('B1')->setValue('Prénom');
        $sheet->getCell('C1')->setValue('Site');
        $sheet->getCell('D1')->setValue('Montant');
        $sheet->getCell('E1')->setValue('Date');
        $sheet->getCell('F1')->setValue('Signature');

        // Increase row cursor after header write
        $sheet->fromArray($list, null, 'A2', true);


        $writer = new Csv($spreadsheet);

        $writer->save('helloworld.csv');

        // return $this->redirectToRoute('home');
        return $this->render('agent/index.html.twig', [
            'controller_name' => 'AgentController',
        ]);
    }
}
