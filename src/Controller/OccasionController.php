<?php

namespace App\Controller;

use DateTime;
use App\Entity\Agent;
use App\Entity\Enfant;
use App\Entity\Occasion;
use App\Service\ExcelService;
use App\Repository\AgentRepository;
use App\Repository\EnfantRepository;
use App\Repository\MontantRepository;
use App\Repository\OccasionRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[IsGranted('ROLE_ADMIN')]
class OccasionController extends AbstractController
{

    public function __construct(private readonly ExcelService $excelService) {}

    #[Route('/occasion', name: 'app_occasion')]
    public function index(): Response
    {
        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }

    #[Route('/liste_agent', name: 'liste_agent')]
    public function listeAgent(AgentRepository $agentRepository): Response
    {
        $agents = $agentRepository->findAll();

        $list = [];
        foreach ($agents as $agent) {
            $listParent[] = $agent->getSite();
            array_push($listParent, $agent->getMatricule());
            array_push($listParent, $agent->getNom());
            array_push($listParent, $agent->getPrenom());
            array_push($listParent, $this->dateFormatted($agent->getDateNaissance()));
            array_push($listParent, $agent->getEmail());
            array_push($listParent, $agent->getAdresse());
            array_push($listParent, $agent->getCodePostal());
            array_push($listParent, strtoupper($agent->getVille()));
            array_push($listParent, $agent->getTelephone());
            array_push($listParent, $this->dateFormatted($agent->getDateEmbauche()));
            array_push($listParent, $agent->isIsCDD());
            array_push($listParent, $this->dateFormatted($agent->getFinContrat()));
            array_push($listParent, $agent->getAyantDroit());
            array_push($listParent, $this->dateFormatted($agent->getDateNaissanceAD()));
            array_push($listParent, $agent->getStatus());

            foreach ($agent->getEnfant() as $enfant) {

                array_push($listParent, $enfant->getPrenom() . ' ' . $enfant->getNom());
                array_push($listParent, $this->dateFormatted($enfant->getDateNaissance()));
                array_push($listParent, ''); // pour avoir la place pour l'age
            }

            array_push($list, $listParent);
            unset($listParent);
        }

        $this->excelService->getExcelAgent($list, $agents, "Liste-Agent-" . date("Y-m-d") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }


    #[Route('/noel_parent', name: 'noel_parent')]
    public function noelParent(AgentRepository $agentRepository, MontantRepository $montantRepository): Response
    {

        $agents = $agentRepository->getAgent(); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $montant = $montantRepository->findOneBy(['id' => 3]);
        $list = [];
        foreach ($agents as $agent) {
            $list[] = [
                $agent->getNom(),
                $agent->getPrenom(),
                $agent->getSite(),
                $montant->getPrix() . '€',
                '',
                ''
            ];
        }

        $this->excelService->getExcelParent($list, $agents, "Agent_Noel-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }



    #[Route('/fete_parent', name: 'fete_parent')]
    public function feteParent(AgentRepository $agentRepository, MontantRepository $montantRepository): Response
    {

        $agents = $agentRepository->getAgent(true); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $montant = $montantRepository->findOneBy(['id' => 11]);
        $list = [];
        foreach ($agents as $agent) {
            if (count($agent->getEnfant()) > 0)
                $list[] = [
                    $agent->getNom(),
                    $agent->getPrenom(),
                    $agent->getSite(),
                    $montant->getPrix() . '€',
                    '',
                    ''
                ];
        }

        $this->excelService->getExcelParent($list, $agents, "Fetes des Parents-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }



    #[Route('/noel_enfant_10', name: 'noel_enfant_10')]
    public function noelEnfant10(EnfantRepository $enfantRepository, MontantRepository $montantRepository): Response
    {

        $enfants = $enfantRepository->getEnfant16ansNoel(false); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];

        foreach ($enfants as $enfant) {
            $list[] = [
                $enfant->getAgents()->first()->getSite(),
                $enfant->getAgents()->first()->getNom() . ' ' . $enfant->getAgents()->first()->getPrenom(),
                $enfant->getNom(),
                $enfant->getPrenom(),
                $enfant->getDateNaissance()->format('d/m/Y'),
                $this->getAge($enfant->getDateNaissance()),
                '',
                ''
            ];
        }

        $this->excelService->getExcelEnfant($list, $enfants, "Enfant_10_Noel-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }

    #[Route('/ancv', name: 'ancv')]
    public function ancv(AgentRepository $agentRepository, MontantRepository $montantRepository): Response
    {

        $somme = $montantRepository->findOneBy(['id' => 12]);
        //dd($somme);
        $agents = $agentRepository->getAgent(false); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];

        foreach ($agents as $agent) {
            $list[] = [
                $agent->getSite(),
                $agent->getNom(),
                $agent->getPrenom(),
                $this->getSommeANCV($agent, $somme->getPrix()),
                '',
                ''
            ];
        }

        $this->excelService->getExcelANCV($list, $agents, "ANCV-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }


    #[Route('/noel_enfant_16', name: 'noel_enfant_16')]
    public function noelEnfant16(EnfantRepository $enfantRepository, MontantRepository $montantRepository): Response
    {

        $enfants = $enfantRepository->getEnfant16ansNoel(true); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];
        $montant = $montantRepository->findOneBy(['id' => 11]);

        foreach ($enfants as $enfant) {
            $list[] = [
                $enfant->getAgents()->first()->getSite(),
                $enfant->getAgents()->first()->getNom() . ' ' . $enfant->getAgents()->first()->getPrenom(),
                $enfant->getNom(),
                $enfant->getPrenom(),
                $enfant->getDateNaissance()->format('d/m/Y'),
                $this->getAge($enfant->getDateNaissance()),
                $montant->getPrix() . '€',
                '',
                ''
            ];
        }

        $this->excelService->getExcelEnfantCarteNoel($list, $enfants, "Enfant_16_Noel-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }

    #[Route('/rentree_enfant_16', name: 'rentree_enfant_16')]
    public function rentreeEnfant16(EnfantRepository $enfantRepository, MontantRepository $montantRepository): Response
    {

        $enfants = $enfantRepository->getEnfant16ansRentree(true); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];
        // $montant = $montantRepository->findOneBy(['id' => 11]);

        foreach ($enfants as $enfant) {
            $list[] = [
                $enfant->getAgents()->first()->getSite(),
                $enfant->getAgents()->first()->getNom() . ' ' . $enfant->getAgents()->first()->getPrenom(),
                $enfant->getNom(),
                $enfant->getPrenom(),
                $enfant->getDateNaissance()->format('d/m/Y'),
                $this->getAge($enfant->getDateNaissance()),
                '',
                '',
                ''
            ];
        }

        $this->excelService->getExcelEnfantCarteRentree16($list, $enfants, "Enfant_16_rentrée-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }

    #[Route('/rentree_enfant', name: 'rentree_enfant')]
    public function rentreeEnfant(EnfantRepository $enfantRepository, MontantRepository $montantRepository): Response
    {

        $enfants = $enfantRepository->getEnfant16ansRentree(false); //findby(['isActive' => true], ['site' => 'ASC', 'nom' => 'ASC']);
        $list = [];
        // $montant = $montantRepository->findOneBy(['id' => 11]);

        foreach ($enfants as $enfant) {
            $list[] = [
                $enfant->getAgents()->first()->getSite(),
                $enfant->getAgents()->first()->getNom() . ' ' . $enfant->getAgents()->first()->getPrenom(),
                $enfant->getNom(),
                $enfant->getPrenom(),
                $enfant->getDateNaissance()->format('d/m/Y'),
                $this->getAge($enfant->getDateNaissance()),
                '',
                '',
                ''
            ];
        }

        $this->excelService->getExcelEnfantCarteRentree16($list, $enfants, "Enfant_rentrée-" . date("Y") . '.xlsx');

        return $this->render('occasion/index.html.twig', [
            'controller_name' => 'OccasionController',
        ]);
    }


    private function getSommeANCV(Agent $agent, int $somme): int
    {
        //$somme = 300; // somme de base pour chaque agent

        // calcul de l'age des enfants au 1er janvier de l'année courante
        $y25 = (DateTime::createFromFormat("d/m/Y", '01/01/' . date('Y')))->sub(new \DateInterval('P25Y'))->format('Y-m-d');
        $y16 = (DateTime::createFromFormat("d/m/Y", '01/01/' . date('Y')))->sub(new \DateInterval('P16Y'))->format('Y-m-d');
        // dd($y25);
        //On parcourt la liste des enfants pour chaque agent
        foreach ($agent->getEnfant() as $enfant) {
            //On commence par regarder l'age des enfants, si < 25 ans et avec certificat alors on rajoute 100€
            if ($enfant->getDateNaissance()->format('Y-m-d') > $y25 && $enfant->isHasCertificat()) {
                // dd($enfant->getDateNaissance()->format('Y-m-d'));
                $somme += 100;
            }
            //On commence par regarder l'age des enfants, si < 16 ans alors on rajoute 100€
            if ($enfant->getDateNaissance()->format('Y-m-d') > $y16) {
                // dd($enfant->getDateNaissance()->format('Y-m-d'));
                $somme += 100;
            }
        }
        return $somme;
    }

    private function dateFormatted(?DateTime $date)
    {
        if (!is_null($date)) {
            return $date->format("d/m/Y");
        }

        return '';
    }

    private function getAge(DateTime $dateNaissance): string
    {

        $aujourdhui = date("Y-m-d");
        $diff = date_diff($dateNaissance, date_create($aujourdhui));
        return $diff->format('%y,%m');
    }
}
