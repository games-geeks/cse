<?php
// src/Controller/CommandeController.php

namespace App\Controller;

use App\Entity\Salle;
use App\Entity\Cinema;
use App\Entity\Commande;
use App\Form\CommandeType;
use App\Entity\SalleCinema;
use App\Entity\GroupeCinema;
use App\Entity\StatutCommande;
use Doctrine\ORM\EntityManager;
use App\Service\SendEmailService;
use App\Repository\AgentRepository;
use App\Security\Voter\CommandeVoter;
use App\Repository\CommandeRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\GroupeCinemaRepository;
use Symfony\Bundle\SecurityBundle\Security;
use App\Repository\StatutCommandeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/commande')]
#[IsGranted('ROLE_AGENT')]
class CommandeController extends AbstractController
{
    #[Route('/cinema', name: 'commande.cinema', methods: ['GET'])]
    public function choixCinema(GroupeCinemaRepository $groupCineRepository): Response
    {
        $cinemas = $groupCineRepository->findAll();
        return $this->render('commande/choixCinema.html.twig', [
            'cinemas' => $cinemas,
        ]);
    }

    #[Route('/{id}/place', name: 'commande.place', methods: ['GET'])]
    public function choixPlace(GroupeCinema $groupCine, AgentRepository $agentRepository): Response
    {
        if ($this->getUser())
            $agent = $agentRepository->findOneBy(['matricule' => $this->getUser()->getMatricule()]);
        return $this->render('commande/choixPlace.html.twig', [
            'cinema' => $groupCine,
            'agent' => $agent,
        ]);
    }

    #[Route('/{id}/recapitulatif', name: 'commande.recapitulatif', methods: ['GET', 'POST'])]
    public function recapitulatif(Request $request, GroupeCinema $groupCine, AgentRepository $agentRepository): Response
    {
        // dd($request, $groupCine);
        $quantity = $request->request->get('quantity');
        $unitPrice = $request->request->get('unit-price');
        $total = $request->request->get('total');
        $freePlaces = $request->request->get('free-places') - $quantity;
        if ($freePlaces < 0) { //on ne peut pas être en négatif
            $freePlaces = 0;
        }
        if ($this->getUser())
            $agent = $agentRepository->findOneBy(['matricule' => $this->getUser()->getMatricule()]);
        return $this->render('commande/recapitulatif.html.twig', [
            'cinema' => $groupCine,
            'agent' => $agent,
            'quantity' => $quantity,
            'unitPrice' => $unitPrice,
            'total' => $total,
            'freePlaces' => $freePlaces,
        ]);
    }


    #[Route('/{id}/validation', name: 'commande.validation', methods: ['GET', 'POST'])]
    public function validation(
        Request $request,
        GroupeCinema $groupCine,
        AgentRepository $agentRepository,
        SendEmailService $mail,
        StatutCommandeRepository $sCR,
        EntityManagerInterface $em,
        CommandeRepository $commandeRepository
    ): Response {
        // dd($request);

        $agent = $agentRepository->findOneBy(['matricule' => $this->getUser()->getMatricule()]);
        $quantity = $request->request->get('quantity');
        $unitPrice = $request->request->get('unit-price');
        $total = $request->request->get('total');
        $freePlaces = $agent->getNbPlaceCinema() - $request->request->get('free-places');


        //Ajoute la commande dans la base de données
        $commande =  new Commande();
        $commande->setAgent($this->getUser());
        $commande->setCinema($groupCine);
        $commande->setNombrePlaces($quantity);
        $commande->setPrixUnitaire($unitPrice * 100);
        $commande->setTarif($total * 100);
        $commande->setFreePlace($freePlaces);
        $commande->setStatutCommande($sCR->findOneBy(['id' => 1]));
        $em->persist($commande);
        $em->flush();
        //Mettre à jour le nombre de places restantes de l'agent
        if ($agent->getNbPlaceCinema() > 0) {
            if ($agent->getNbPlaceCinema() - $quantity > 0)
                $agent->setNbPlaceCinema($agent->getNbPlaceCinema() - $quantity);
            else
                $agent->setNbPlaceCinema(0);
            $em->persist($agent);
            $em->flush();
        }
        // dd($request, $groupCine);
        //Envoie un mail au CSE
        $mail->send(
            'no-reply@assurance-maladie.fr',
            'cse@assurance-maladie.fr',
            'Nouvelle commande de cinéma',
            'commande_cse',
            compact('agent', 'quantity', 'unitPrice', 'total', 'freePlaces', 'groupCine') // ['user' => $user, 'token'=>$token]
        );

        //Envoie un mail à l'agent
        $mail->send(
            'no-reply@assurance-maladie.fr',
            $this->getUser()->getEmail(),
            'Récapitulatif de la commande de places',
            'commande_agent',
            compact('agent', 'quantity', 'unitPrice', 'total', 'freePlaces', 'groupCine') // ['user' => $user, 'token'=>$token]
        );
        return $this->render('home/index.html.twig', ['agent' => $agent, 'commandes' => $commandeRepository->findAll()]);
    }

    #[Route('/viewAll', name: 'commande.viewAll', methods: ['GET'])]
    public function viewAll(
        Request $request,
        CommandeRepository $commandeRepository,
        StatutCommandeRepository $statutCommandeRepository,
        Security $security
    ): Response {

        $page = $request->query->getInt('page', 1);
        $userId = $security->getUser()->getId();
        # Tout afficher si on est membre du CSE, sinon on affiche uniquement les commandes de l'agent
        $canListAll = $security->isGranted(CommandeVoter::LIST_ALL);
        $commandes = $commandeRepository->paginateCommandes($page, $canListAll ? null : $userId);
        return $this->render('commande/voirToutes.html.twig', [
            'commandes' => $commandes,
            'statutCommande' => $statutCommandeRepository->findAll()
        ]);
    }
    #[Route('/{id}/change', name: 'commande.change.statut', methods: ['POST'])]
    public function changeStatut(
        Request $request,
        AgentRepository $agentRepository,
        Commande $commande,
        EntityManagerInterface $entityManager,
        SendEmailService $mail,
        Security $security
    ) {
        $agent = $agentRepository->findOneBy(['matricule' => $this->getUser()->getMatricule()]);
        //On commande par regarder si le statut a changé
        $isCSE = $security->isGranted(CommandeVoter::LIST_ALL);
        if ($isCSE) {
            $statut = $request->request->get('nouveauStatut');
            $newStatut = $entityManager->getRepository(StatutCommande::class)->find($statut);
            if ($statut != $commande->getStatutCommande()) {
                //On vérifie si l'utilisateur est membre du CSE

                //On prépare le mail selon le statut
                //Envoie un mail au CSE
                if ($newStatut->getId() == 5) { //attente de paiement
                    $mail->send(
                        'no-reply@assurance-maladie.fr',
                        $commande->getAgent()->getEmail(),
                        'Paiement de la commande de cinéma en attente',
                        'commande_paiement',
                        compact('agent', 'commande') // ['user' => $user, 'token'=>$token]

                    );
                }
                //enfin on change le statut
                $commande->setStatutCommande($newStatut);
                $entityManager->persist($commande);
                $entityManager->flush();
            }
        } else {
            //on annule la commande
            $newStatut = $entityManager->getRepository(StatutCommande::class)->find(4);
            $commande->setStatutCommande($newStatut);
            $entityManager->persist($commande);

            //et il faut recréditer le nombre de places restantes
            $agent->setNbPlaceCinema($agent->getNbPlaceCinema() + $commande->getFreePlace());
            $entityManager->persist($agent);
            $entityManager->flush();
        }

        return $this->redirectToRoute('commande.viewAll');
    }

    public function newOld(Request $request, EntityManagerInterface $em): Response
    {
        $commande = new Commande();
        $form = $this->createForm(CommandeType::class, $commande);

        // Récupérer les cinémas et la liste des salles dynamiquement en fonction de la sélection
        $cinemas = $em->getRepository(Cinema::class)->findAll();
        $salles = [];
        $theSalle = null;
        $theCinema = null;
        $tarif = 0;

        // Si un cinéma est sélectionné, récupère les salles associées
        if ($request->request->get('cinema')) {
            // dd($request->request->get('cinema'));
            $cinema = $em->getRepository(Cinema::class)->find($request->request->get('cinema'));
            $theCinema = $cinema;
            if ($cinema) {
                $salles = $cinema->getSalleCinemas();
            }
        }
        // dd($request);
        if ($request->request->get('commande[salle]')) {
            $salle = $em->getRepository(SalleCinema::class)->find($request->request->get('commande[salle]'));
            dd($salle);
            $theSalle = $salle;
            if ($salle) {
                $tarif = $salle->getPrix();
            }
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $em->persist($commande);
            $em->flush();

            $this->addFlash('success', 'Votre commande a été enregistrée avec succès.');

            return $this->redirectToRoute('commande_new');
        }

        return $this->render('commande/new.html.twig', [
            'form' => $form->createView(),
            'cinemas' => $cinemas,
            'theCinema' => $theCinema,
            'salles' => $salles,
            'theSalle' => $theSalle,
            'tarif' => $tarif,
        ]);
    }
}
