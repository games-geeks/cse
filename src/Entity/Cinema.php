<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\CinemaRepository;

#[ORM\Entity(repositoryClass: CinemaRepository::class)]
#[ORM\HasLifecycleCallbacks]

class Cinema
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $adresse = null;

    #[ORM\Column(length: 255)]
    private ?string $ville = null;

    #[ORM\Column(length: 255)]
    private ?string $codePostal = null;

    #[ORM\ManyToOne(inversedBy: 'cinemas')]
    private ?GroupeCinema $groupeCinema = null;

    /**
     * @var Collection<int, SalleCinema>
     */
    #[ORM\OneToMany(mappedBy: 'cinema', targetEntity: SalleCinema::class)]
    private Collection $salleCinemas;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $urlAlloCine = null;

    public function __construct()
    {
        $this->salleCinemas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): static
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): static
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getGroupeCinema(): ?GroupeCinema
    {
        return $this->groupeCinema;
    }

    public function setGroupeCinema(?GroupeCinema $groupeCinema): static
    {
        $this->groupeCinema = $groupeCinema;

        return $this;
    }

    /**
     * @return Collection<int, SalleCinema>
     */
    public function getSalleCinemas(): Collection
    {
        return $this->salleCinemas;
    }

    public function addSalleCinema(SalleCinema $salleCinema): static
    {
        if (!$this->salleCinemas->contains($salleCinema)) {
            $this->salleCinemas->add($salleCinema);
            $salleCinema->setCinema($this);
        }

        return $this;
    }

    public function removeSalleCinema(SalleCinema $salleCinema): static
    {
        if ($this->salleCinemas->removeElement($salleCinema)) {
            // set the owning side to null (unless already changed)
            if ($salleCinema->getCinema() === $this) {
                $salleCinema->setCinema(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getNom();
    }

    public function getUrlAlloCine(): ?string
    {
        return $this->urlAlloCine;
    }

    public function setUrlAlloCine(?string $urlAlloCine): static
    {
        $this->urlAlloCine = $urlAlloCine;

        return $this;
    }
}
