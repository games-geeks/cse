<?php

namespace App\Entity;

use App\Repository\MontantRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MontantRepository::class)]
class Montant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column]
    private ?int $prix = null;


    #[ORM\ManyToOne(inversedBy: 'montants')]
    private ?Raison $raison = null;

    #[ORM\ManyToOne(inversedBy: 'montants')]
    private ?Restriction $condition = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): static
    {
        $this->prix = $prix;

        return $this;
    }


    public function getRaison(): ?Raison
    {
        return $this->raison;
    }

    public function setRaison(?Raison $raison): static
    {
        $this->raison = $raison;

        return $this;
    }

    public function getCondition(): ?Restriction
    {
        return $this->condition;
    }

    public function setCondition(?Restriction $condition): static
    {
        $this->condition = $condition;

        return $this;
    }
}
