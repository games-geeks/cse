<?php

namespace App\Entity;

use App\Repository\RestrictionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RestrictionRepository::class)]
class Restriction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $condition = null;

    #[ORM\OneToMany(mappedBy: 'condition', targetEntity: Montant::class)]
    private Collection $montants;

    public function __construct()
    {
        $this->montants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCondition(): ?string
    {
        return $this->condition;
    }

    public function setCondition(string $condition): static
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return Collection<int, Montant>
     */
    public function getMontants(): Collection
    {
        return $this->montants;
    }

    public function addMontant(Montant $montant): static
    {
        if (!$this->montants->contains($montant)) {
            $this->montants->add($montant);
            $montant->setCondition($this);
        }

        return $this;
    }

    public function removeMontant(Montant $montant): static
    {
        if ($this->montants->removeElement($montant)) {
            // set the owning side to null (unless already changed)
            if ($montant->getCondition() === $this) {
                $montant->setCondition(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }
}
