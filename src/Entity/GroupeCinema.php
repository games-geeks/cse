<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\GroupeCinemaRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: GroupeCinemaRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Vich\Uploadable]
class GroupeCinema
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $logo = null;

    #[Vich\UploadableField(mapping: 'logo', fileNameProperty: 'logo')]
    private ?File $imageFile = null;

    /**
     * @var Collection<int, Cinema>
     */
    #[ORM\OneToMany(mappedBy: 'groupeCinema', targetEntity: Cinema::class)]
    private Collection $cinemas;

    #[ORM\Column(nullable: true)]
    private ?float $tarifNormal = null;

    #[ORM\Column(nullable: true)]
    private ?float $tarifSpecial = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $condition = null;

    public function __construct()
    {
        $this->cinemas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): static
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;
        if (null !== $imageFile) {

            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            //$this->setUpdatedAt(new \DateTimeImmutable);
            $this->setUpdatedAt(new \DateTimeImmutable);
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return Collection<int, Cinema>
     */
    public function getCinemas(): Collection
    {
        return $this->cinemas;
    }

    public function addCinema(Cinema $cinema): static
    {
        if (!$this->cinemas->contains($cinema)) {
            $this->cinemas->add($cinema);
            $cinema->setGroupeCinema($this);
        }

        return $this;
    }

    public function removeCinema(Cinema $cinema): static
    {
        if ($this->cinemas->removeElement($cinema)) {
            // set the owning side to null (unless already changed)
            if ($cinema->getGroupeCinema() === $this) {
                $cinema->setGroupeCinema(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    public function getTarifNormal(): ?float
    {
        return $this->tarifNormal;
    }

    public function setTarifNormal(?float $tarifNormal): static
    {
        $this->tarifNormal = $tarifNormal;

        return $this;
    }

    public function getTarifSpecial(): ?float
    {
        return $this->tarifSpecial;
    }

    public function setTarifSpecial(?float $tarifSpecial): static
    {
        $this->tarifSpecial = $tarifSpecial;

        return $this;
    }

    public function getCondition(): ?string
    {
        return $this->condition;
    }

    public function setCondition(?string $condition): static
    {
        $this->condition = $condition;

        return $this;
    }
}
