<?php

namespace App\Entity;

use App\Repository\RaisonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RaisonRepository::class)]
class Raison
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\OneToMany(mappedBy: 'raison', targetEntity: Montant::class)]
    private Collection $montants;

    #[ORM\OneToMany(mappedBy: 'raison', targetEntity: Occasion::class)]
    private Collection $occasions;

    public function __construct()
    {
        $this->montants = new ArrayCollection();
        $this->occasions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Montant>
     */
    public function getMontants(): Collection
    {
        return $this->montants;
    }

    public function addMontant(Montant $montant): static
    {
        if (!$this->montants->contains($montant)) {
            $this->montants->add($montant);
            $montant->setRaison($this);
        }

        return $this;
    }

    public function removeMontant(Montant $montant): static
    {
        if ($this->montants->removeElement($montant)) {
            // set the owning side to null (unless already changed)
            if ($montant->getRaison() === $this) {
                $montant->setRaison(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    /**
     * @return Collection<int, Occasion>
     */
    public function getOccasions(): Collection
    {
        return $this->occasions;
    }

    public function addOccasion(Occasion $occasion): static
    {
        if (!$this->occasions->contains($occasion)) {
            $this->occasions->add($occasion);
            $occasion->setRaison($this);
        }

        return $this;
    }

    public function removeOccasion(Occasion $occasion): static
    {
        if ($this->occasions->removeElement($occasion)) {
            // set the owning side to null (unless already changed)
            if ($occasion->getRaison() === $this) {
                $occasion->setRaison(null);
            }
        }

        return $this;
    }
}
