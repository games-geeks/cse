<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\AgentRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: AgentRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Agent
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $email = null;


    #[ORM\Column(nullable: true)]
    private ?int $matricule = null;

    #[ORM\Column(length: 255)]
    private ?string $prenom = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateEmbauche = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adresse = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $codePostal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ville = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephone = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ayantDroit = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateNaissanceAD = null;

    #[ORM\ManyToOne(inversedBy: 'agents')]
    private ?Site $site = null;

    #[ORM\ManyToOne(inversedBy: 'agents')]
    private ?StatusMarital $status = null;

    #[ORM\ManyToMany(targetEntity: Enfant::class, inversedBy: 'agents')]
    private Collection $enfant;

    #[ORM\Column(nullable: true)]
    private ?bool $isActive = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $finContrat = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isCDD = null;

    #[ORM\Column(nullable: true)]
    private ?int $nbPlaceCinema = 0;

    public function __construct()
    {
        $this->enfant = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }


    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(?int $matricule): static
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateEmbauche(): ?\DateTimeInterface
    {
        return $this->dateEmbauche;
    }

    public function setDateEmbauche(\DateTimeInterface $dateEmbauche): static
    {
        $this->dateEmbauche = $dateEmbauche;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): static
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): static
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): static
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): static
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getAyantDroit(): ?string
    {
        return $this->ayantDroit;
    }

    public function setAyantDroit(?string $ayantDroit): static
    {
        $this->ayantDroit = $ayantDroit;

        return $this;
    }

    public function getDateNaissanceAD(): ?\DateTimeInterface
    {
        return $this->dateNaissanceAD;
    }

    public function setDateNaissanceAD(?\DateTimeInterface $dateNaissanceAD): static
    {
        $this->dateNaissanceAD = $dateNaissanceAD;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): static
    {
        $this->site = $site;

        return $this;
    }

    public function getStatus(): ?StatusMarital
    {
        return $this->status;
    }

    public function setStatus(?StatusMarital $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Enfant>
     */
    public function getEnfant(): Collection
    {
        return $this->enfant;
    }

    public function addEnfant(Enfant $enfant): static
    {
        if (!$this->enfant->contains($enfant)) {
            $this->enfant->add($enfant);
        }

        return $this;
    }

    public function removeEnfant(Enfant $enfant): static
    {
        $this->enfant->removeElement($enfant);

        return $this;
    }

    public function __toString()
    {
        return $this->getPrenom() . '  ' . $this->getNom();
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): static
    {
        $this->isActive = $isActive;

        return $this;
    }


    public function getFinContrat(): ?\DateTimeInterface
    {
        return $this->finContrat;
    }

    public function setFinContrat(?\DateTimeInterface $finContrat): static
    {
        $this->finContrat = $finContrat;

        return $this;
    }

    public function isIsCDD(): ?bool
    {
        return $this->isCDD;
    }

    public function setIsCDD(?bool $isCDD): static
    {
        $this->isCDD = $isCDD;

        return $this;
    }

    public function getNbPlaceCinema(): ?int
    {
        return $this->nbPlaceCinema;
    }

    public function setNbPlaceCinema(?int $nbPlaceCinema): static
    {
        $this->nbPlaceCinema = $nbPlaceCinema;

        return $this;
    }
}
