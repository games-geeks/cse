<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\Agent;
use App\Entity\StatusMarital;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('matricule')
            ->add('prenom')
            ->add('nom')
            ->add('dateEmbauche', null, [
                'widget' => 'single_text',
            ])
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telephone')
            ->add('dateNaissance', null, [
                'widget' => 'single_text',
            ])
            ->add('ayantDroit')
            ->add('dateNaissanceAD', null, [
                'widget' => 'single_text',
            ])
            ->add('isActive')
            ->add('finContrat', null, [
                'widget' => 'single_text',
            ])
            ->add('isCDD')

            ->add('site', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'nom',
            ])
            ->add('status', EntityType::class, [
                'class' => StatusMarital::class,
                'choice_label' => 'nom',
            ])
            ->add('enfant', CollectionType::class, [
                'entry_type' => EnfantType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options' => ['label' => false],
                'attr' => [
                    'data-controller' => 'form-collection'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agent::class,
        ]);
    }
}
