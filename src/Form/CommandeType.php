<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Cinema;
use App\Entity\Commande;
use App\Entity\SalleCinema;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class CommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('cinema', EntityType::class, [
                'class' => Cinema::class,
                'choice_label' => 'nom',
                'placeholder' => 'Sélectionnez un cinéma',
                'mapped' => false, // Ce champ n'est pas directement lié à l'entité Commande
            ])
            ->add('salleCinema', EntityType::class, [
                'class' => SalleCinema::class,
                'choice_label' => 'nom',
                'placeholder' => 'Sélectionnez une salle',
                'mapped' => true,
                'required' => false, // On le rend optionnel pour la première soumission
            ])
            ->add('nombrePlaces', IntegerType::class, [
                'label' => 'Nombre de places',
                'attr' => ['min' => 1, 'max' => 10],
            ])
            ->add('tarif', MoneyType::class, [
                'label' => 'Tarif total',
                'currency' => 'EUR',
                'mapped' => false, // Ce champ n'est pas lié à l'entité car il est calculé
                'disabled' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commande::class,
        ]);
    }
}
