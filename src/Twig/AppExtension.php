<?php

namespace App\Twig;

use App\Entity\Console;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Repository\TarifRepository;
use Doctrine\ORM\Mapping\Entity;
use Twig\Extension\AbstractExtension;
use Doctrine\ORM\PersistentCollection;

class AppExtension extends AbstractExtension
{



    public function __construct() {}
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }


    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('entityToArray', [$this, 'entityToArray']),
        ];
    }

    /**
     * @param int count le nombre à controler
     * @param string singular le singuler
     * @param plural optionnel
     *
     * @return string
     */
    public function pluralize(int $count, string $singular, ?string $plural = null): string
    {
        /* Si pas de pluriel, on le contruit*/
        $plural ??=  $singular . 's';
        /* On regarde selon le nombre ce que l'on doit retourner*/
        $res = $count > 1 ? $plural : $singular;

        return $count . ' ' . $res;
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function entityToArray($entity): string
    {
        $tempArray = array();
        foreach ($entity as $ent) {
            array_push($tempArray, $ent);
        }
        return implode(", ", $tempArray);
    }
}
