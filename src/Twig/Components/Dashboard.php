<?php

namespace App\Twig\Components;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;


#[AsLiveComponent]
class Dashboard
{
    use DefaultActionTrait;
    #[LiveProp()]
    public ?string $entity = null;

    public array $lines = [];
    #[LiveProp()]
    public array $headers = [];
    #[LiveProp()]
    public string $currentHeaders = '';
    #[LiveProp()]
    public string $currentDirection = 'asc';

    public function __construct(private readonly EntityManagerInterface $em) {}

    public function mount(string $entity, array $headers): void
    {
        $repository = $this->em->getRepository($entity);
        $this->lines = $repository->findAll();
        $this->headers = $headers;
        $this->entity = $entity;
        if (empty($this->headers)) {
            throw new \Exception('Aucun header');
        }
        $this->currentHeaders = $this->headers[0];
    }

    #[LiveAction]
    public function sort(#[LiveArg] string $header, #[LiveArg] string $direction): void
    {
        $repository = $this->em->getRepository($this->entity);
        $this->lines = $repository->findBy([],  [$header => $direction]);
    }
}
