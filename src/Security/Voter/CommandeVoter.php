<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class CommandeVoter extends Voter
{
    public const EDIT = 'COMMANDE_EDIT';
    public const VIEW = 'COMMANDE_VIEW';
    public const LIST_ALL = 'COMMANDE_LIST_ALL';

    public function __construct(private readonly Security $security) {}

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return  in_array($attribute, [self::LIST_ALL])
            || (
                in_array($attribute, [self::EDIT, self::VIEW])
                && $subject instanceof \App\Entity\Commande
            );
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                // logic to determine if the user can EDIT
                // return true or false
                break;

            case self::VIEW:
                // logic to determine if the user can VIEW
                // return true or false
                break;
            case self::LIST_ALL:
                // logic to determine if the user can LIST_ALL
                if ($this->security->isGranted('ROLE_CSE')) {
                    return true;
                }
                // return true or false
                break;
        }

        return false;
    }
}
