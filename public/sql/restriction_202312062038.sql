INSERT INTO public.restriction (id,nom,"condition") VALUES
	 (1,'Tous les agents','Aucune'),
	 (2,'Noel Enfant','Age enfant >= 10 ans et <= 16 ans'),
	 (3,'Agents Parent','Agent ayant au moins 1 enfant'),
	 (4,'Enfant de moins de 16 ans','Age de l''enfant < 16 ans au 01/09 de l''année'),
	 (5,'Rentrée en Maternelle','Age >3 ans et en Maternelle'),
	 (6,'Rentrée en Primaire','Enfant en primaire'),
	 (7,'Rentrée au Collège','Enfant au Collège'),
	 (8,'Rentrée au Lycée','Enfant au Lycée'),
	 (9,'Rentrée à l''université','Enfant à l''université');
