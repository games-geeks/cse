<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241111203854 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE salle_cinema (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, cinema_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, type_salle VARCHAR(255) NOT NULL, places_restantes INTEGER NOT NULL, CONSTRAINT FK_E000D84BB4CB84B6 FOREIGN KEY (cinema_id) REFERENCES cinema (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_E000D84BB4CB84B6 ON salle_cinema (cinema_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE salle_cinema');
    }
}
