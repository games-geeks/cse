<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241114192724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE film (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, realisateur VARCHAR(255) NOT NULL, acteurs VARCHAR(255) NOT NULL, duree VARCHAR(255) NOT NULL, affiche VARCHAR(255) NOT NULL, synopsys VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE horaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, cinema_id INTEGER DEFAULT NULL, film_id INTEGER DEFAULT NULL, sceance VARCHAR(255) NOT NULL, CONSTRAINT FK_BBC83DB6B4CB84B6 FOREIGN KEY (cinema_id) REFERENCES cinema (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_BBC83DB6567F5183 FOREIGN KEY (film_id) REFERENCES film (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_BBC83DB6B4CB84B6 ON horaire (cinema_id)');
        $this->addSql('CREATE INDEX IDX_BBC83DB6567F5183 ON horaire (film_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE horaire');
    }
}
