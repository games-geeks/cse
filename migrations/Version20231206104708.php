<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231206104708 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE agent (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, status_id INTEGER DEFAULT NULL, email VARCHAR(180) DEFAULT NULL, matricule INTEGER DEFAULT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(10) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, date_naissance DATE DEFAULT NULL, ayant_droit VARCHAR(255) DEFAULT NULL, date_naissance_ad DATE DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , CONSTRAINT FK_268B9C9DF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_268B9C9D6BF700BD FOREIGN KEY (status_id) REFERENCES status_marital (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_268B9C9DF6BD1646 ON agent (site_id)');
        $this->addSql('CREATE INDEX IDX_268B9C9D6BF700BD ON agent (status_id)');
        $this->addSql('CREATE TABLE agent_enfant (agent_id INTEGER NOT NULL, enfant_id INTEGER NOT NULL, PRIMARY KEY(agent_id, enfant_id), CONSTRAINT FK_6A505B843414710B FOREIGN KEY (agent_id) REFERENCES agent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6A505B84450D2529 FOREIGN KEY (enfant_id) REFERENCES enfant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_6A505B843414710B ON agent_enfant (agent_id)');
        $this->addSql('CREATE INDEX IDX_6A505B84450D2529 ON agent_enfant (enfant_id)');
        $this->addSql('CREATE TABLE enfant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_naissance DATE NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('CREATE TABLE membre (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6B4FB29E7927C74 ON membre (email)');
        $this->addSql('CREATE TABLE montant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, raison_id INTEGER DEFAULT NULL, condition_id INTEGER DEFAULT NULL, prix INTEGER NOT NULL, CONSTRAINT FK_A9F3E328EE1E550F FOREIGN KEY (raison_id) REFERENCES raison (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_A9F3E328887793B6 FOREIGN KEY (condition_id) REFERENCES restriction (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_A9F3E328EE1E550F ON montant (raison_id)');
        $this->addSql('CREATE INDEX IDX_A9F3E328887793B6 ON montant (condition_id)');
        $this->addSql('CREATE TABLE occasion (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, raison_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, annee VARCHAR(255) NOT NULL, CONSTRAINT FK_8A66DCE5EE1E550F FOREIGN KEY (raison_id) REFERENCES raison (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_8A66DCE5EE1E550F ON occasion (raison_id)');
        $this->addSql('CREATE TABLE raison (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE restriction (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, condition VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE site (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('CREATE TABLE status_marital (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('CREATE TABLE messenger_messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, body CLOB NOT NULL, headers CLOB NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , available_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , delivered_at DATETIME DEFAULT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE agent');
        $this->addSql('DROP TABLE agent_enfant');
        $this->addSql('DROP TABLE enfant');
        $this->addSql('DROP TABLE membre');
        $this->addSql('DROP TABLE montant');
        $this->addSql('DROP TABLE occasion');
        $this->addSql('DROP TABLE raison');
        $this->addSql('DROP TABLE restriction');
        $this->addSql('DROP TABLE site');
        $this->addSql('DROP TABLE status_marital');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
