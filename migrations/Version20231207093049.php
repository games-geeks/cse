<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231207093049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agent ADD COLUMN contrat VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE agent ADD COLUMN fin_contrat DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__agent AS SELECT id, site_id, status_id, email, matricule, prenom, nom, date_embauche, adresse, code_postal, ville, telephone, date_naissance, ayant_droit, date_naissance_ad, is_active, creeatedAt, updatedAt FROM agent');
        $this->addSql('DROP TABLE agent');
        $this->addSql('CREATE TABLE agent (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, status_id INTEGER DEFAULT NULL, email VARCHAR(180) DEFAULT NULL, matricule INTEGER DEFAULT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(10) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, date_naissance DATE DEFAULT NULL, ayant_droit VARCHAR(255) DEFAULT NULL, date_naissance_ad DATE DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , CONSTRAINT FK_268B9C9DF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_268B9C9D6BF700BD FOREIGN KEY (status_id) REFERENCES status_marital (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO agent (id, site_id, status_id, email, matricule, prenom, nom, date_embauche, adresse, code_postal, ville, telephone, date_naissance, ayant_droit, date_naissance_ad, is_active, creeatedAt, updatedAt) SELECT id, site_id, status_id, email, matricule, prenom, nom, date_embauche, adresse, code_postal, ville, telephone, date_naissance, ayant_droit, date_naissance_ad, is_active, creeatedAt, updatedAt FROM __temp__agent');
        $this->addSql('DROP TABLE __temp__agent');
        $this->addSql('CREATE INDEX IDX_268B9C9DF6BD1646 ON agent (site_id)');
        $this->addSql('CREATE INDEX IDX_268B9C9D6BF700BD ON agent (status_id)');
    }
}
