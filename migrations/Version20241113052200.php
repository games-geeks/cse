<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241113052200 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cinema ADD COLUMN url_allo_cine VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__cinema AS SELECT id, groupe_cinema_id, nom, adresse, ville, code_postal, creeatedAt, updatedAt FROM cinema');
        $this->addSql('DROP TABLE cinema');
        $this->addSql('CREATE TABLE cinema (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, groupe_cinema_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , CONSTRAINT FK_D48304B4E45F5781 FOREIGN KEY (groupe_cinema_id) REFERENCES groupe_cinema (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO cinema (id, groupe_cinema_id, nom, adresse, ville, code_postal, creeatedAt, updatedAt) SELECT id, groupe_cinema_id, nom, adresse, ville, code_postal, creeatedAt, updatedAt FROM __temp__cinema');
        $this->addSql('DROP TABLE __temp__cinema');
        $this->addSql('CREATE INDEX IDX_D48304B4E45F5781 ON cinema (groupe_cinema_id)');
    }
}
