<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241121161902 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE groupe_cinema ADD COLUMN tarif_normal DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE groupe_cinema ADD COLUMN tarif_special DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE groupe_cinema ADD COLUMN condition VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__groupe_cinema AS SELECT id, nom, logo, creeatedAt, updatedAt FROM groupe_cinema');
        $this->addSql('DROP TABLE groupe_cinema');
        $this->addSql('CREATE TABLE groupe_cinema (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, logo VARCHAR(255) NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('INSERT INTO groupe_cinema (id, nom, logo, creeatedAt, updatedAt) SELECT id, nom, logo, creeatedAt, updatedAt FROM __temp__groupe_cinema');
        $this->addSql('DROP TABLE __temp__groupe_cinema');
    }
}
