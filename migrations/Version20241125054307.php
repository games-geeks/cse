<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241125054307 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commande ADD COLUMN free_place INTEGER DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__commande AS SELECT id, cinema_id, agent_id, statut_commande_id, nombre_places, tarif, prix_unitaire, creeatedAt, updatedAt FROM commande');
        $this->addSql('DROP TABLE commande');
        $this->addSql('CREATE TABLE commande (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, cinema_id INTEGER NOT NULL, agent_id INTEGER NOT NULL, statut_commande_id INTEGER NOT NULL, nombre_places INTEGER NOT NULL, tarif DOUBLE PRECISION NOT NULL, prix_unitaire DOUBLE PRECISION NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL --(DC2Type:datetime_immutable)
        , CONSTRAINT FK_6EEAA67DB4CB84B6 FOREIGN KEY (cinema_id) REFERENCES groupe_cinema (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6EEAA67D3414710B FOREIGN KEY (agent_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6EEAA67DFB435DFD FOREIGN KEY (statut_commande_id) REFERENCES statut_commande (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO commande (id, cinema_id, agent_id, statut_commande_id, nombre_places, tarif, prix_unitaire, creeatedAt, updatedAt) SELECT id, cinema_id, agent_id, statut_commande_id, nombre_places, tarif, prix_unitaire, creeatedAt, updatedAt FROM __temp__commande');
        $this->addSql('DROP TABLE __temp__commande');
        $this->addSql('CREATE INDEX IDX_6EEAA67DB4CB84B6 ON commande (cinema_id)');
        $this->addSql('CREATE INDEX IDX_6EEAA67D3414710B ON commande (agent_id)');
        $this->addSql('CREATE INDEX IDX_6EEAA67DFB435DFD ON commande (statut_commande_id)');
    }
}
