const quantityInput = document.getElementById('quantity');
const totalPrice = document.getElementById('total');
const totalHidden = document.getElementById('total-hidden');
const freePlacesInput = document.getElementById('free-places'); // Nombre de places gratuites
const freePlaces = parseInt(freePlacesInput.value);
const unitPriceInput = document.getElementById('unit-price'); // Prix unitaire après épuisement des places gratuites
const unitPrice = parseFloat(unitPriceInput.value);


quantityInput.addEventListener('input', () => {
    const quantity = parseInt(quantityInput.value);
    let total = 0;

    if (quantity <= freePlaces) {
        total = 0;
    } else {
        total = (quantity - freePlaces) * unitPrice;
    } totalPrice.textContent = total.toFixed(2);
    document.getElementById('total-hidden').value = total;
});
